from django.apps import AppConfig


class SportsnewsappConfig(AppConfig):
    name = 'sportsnewsapp'
