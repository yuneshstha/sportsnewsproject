from django import forms
from .models import *


class NewsCommentForm(forms.Form):
    comment = forms.CharField(widget=forms.Textarea(attrs={
        'placeholder': 'Write your comment...'
    }))
    commentername = forms.CharField(widget=forms.TextInput(attrs={
        'placeholder': 'Name...'
    }))
    email = forms.CharField(widget=forms.EmailInput(attrs={
        'placeholder': 'Enter your valid email...'
    }))
