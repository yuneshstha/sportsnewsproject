from django.urls import path
from .views import *

app_name = 'sportsnewsapp'
urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('categorydetail/<int:pk>/',
         CategoryDetailView.as_view(), name='categorydetail'),
    path('newsdetail/<int:pk>/',
         NewsDetailView.as_view(), name='newsdetail'),
    path('newscommentcreate/<int:pk>/',
         NewsCommentCreateView.as_view(),
         name='newscommentcreate'),
]
