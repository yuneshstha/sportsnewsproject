from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import render
from django.views.generic import *
from .models import *
from .forms import *
import urllib.request
# import json
# with urllib.request.urlopen("http://api.openweathermap.org/data/2.5/weather?q=Kathmandu&APPID=58fb41d9334be75766dc0684f60fff2f") as url:
#     temp = json.loads(url.read().decode())

#     temp = int(temp['main']['temp'] - 273)


class ClientMixin(object):
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        # context['temp'] = temp
        context['latestnews'] = News.objects.filter(
            deleted_at__isnull=True).order_by('-id')
        context['categories'] = Category.objects.filter(
            deleted_at__isnull=True)
        context['comments'] = Comment.objects.filter(
            deleted_at__isnull=True).order_by('-id')
        context['breaking_news'] = News.objects.filter(
            deleted_at__isnull=True,
            category__title='Breaking News').order_by('-id')
        return context


class HomeView(ClientMixin, TemplateView):
    template_name = 'clienttemplates/home.html'


class NewsDetailView(ClientMixin, DetailView):
    template_name = 'clienttemplates/newsdetail.html'
    model = News
    context_object_name = 'news'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        self.id = self.kwargs['pk']
        news = News.objects.get(id=self.id)
        viewcount = news.viewed + 1
        news.viewed = viewcount
        news.save()
        context['form'] = NewsCommentForm
        return context

    # def get(self, request, *args, **kwargs):
    #     self.id = self.kwargs['pk']
    #     news = News.objects.get(id=self.id)
    #     viewcount = news.viewed + 1
    #     news.viewed = viewcount
    #     news.save()
    #     return super().get(request, *args, **kwargs)


class CategoryDetailView(ClientMixin, DetailView):
    template_name = 'clienttemplates/categorydetail.html'
    model = Category
    context_object_name = 'category'


class NewsCommentCreateView(SuccessMessageMixin, ClientMixin, FormView):
    template_name = 'clienttemplates/newsdetail.html'
    form_class = NewsCommentForm
    success_url = '/'
    success_message = 'Thank you for creating waiyati comment here. '

    def dispatch(self, *args, **kwargs):
        self.id = self.kwargs['pk']
        return super().dispatch(*args, **kwargs)

    def form_valid(self, form):
        self.comment = form.cleaned_data['comment']
        self.commentername = form.cleaned_data['commentername']
        self.news = News.objects.get(id=self.id)
        Comment.objects.create(post=self.news, comment=self.comment,
                               commentername=self.commentername)

        return super().form_valid(form)

    def get_success_url(self, **kwargs):
        url = '/newsdetail/' + str(self.id)
        return url
