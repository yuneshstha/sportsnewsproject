{
    'coord': {'lon': 85.32, 'lat': 27.71},
    'weather': [{
        'id': 803,
        'main': 'Clouds',
        'description': 'broken clouds',
        'icon': '04d'
    }],
    'base': 'stations',
    'main': {
        'temp': 296.15,
        'pressure': 1013,
        'humidity': 64,
        'temp_min': 296.15,
        'temp_max': 296.15
    },
    'visibility': 6000,
    'wind': {'speed': 2.1, 'deg': 320},
    'clouds': {'all': 75},
    'dt': 1525611000,
    'sys': {
        'type': 1,
        'id': 7907,
        'message': 0.0067,
        'country': 'NP',
        'sunrise': 1525563317,
        'sunset': 1525611343},
    'id': 1283240,
    'name': 'Kathmandu',
    'cod': 200
}
